# Recos Diet

Site web présentant les recommandations nutritionnelles pour le patient adulte de réanimation alimenté en nutrition
artificielle.

Cet outil a été élaboré en partenariat avec l'équipe diététique du service de réanimation du CHU de Toulouse.
