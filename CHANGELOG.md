## [2.2.1](https://gitlab.com/ydisanto/recos-diet/compare/2.2.0...2.2.1) (2023-04-14)


### Bug Fixes

* adjusted weight ([46d5412](https://gitlab.com/ydisanto/recos-diet/commit/46d5412045bdab86225f15f526a8950da5fa4a8c))

# [2.2.0](https://gitlab.com/ydisanto/recos-diet/compare/2.1.1...2.2.0) (2023-02-21)


### Features

* add periolimel product ([f4c6aaa](https://gitlab.com/ydisanto/recos-diet/commit/f4c6aaa79ccf483cf9a0f8453f75eb7c22b3d8ad))

## [2.1.1](https://gitlab.com/ydisanto/recos-diet/compare/2.1.0...2.1.1) (2022-11-02)


### Bug Fixes

* update dependency ([071bcc9](https://gitlab.com/ydisanto/recos-diet/commit/071bcc90b0b4af46a38f3b65b3704e92d6ce8734))

# [2.1.0](https://gitlab.com/ydisanto/recos-diet/compare/2.0.2...2.1.0) (2022-10-07)


### Bug Fixes

* survey link ([a09cb9e](https://gitlab.com/ydisanto/recos-diet/commit/a09cb9eca41d9373c3120126f7ddf6bdd6fa6885))


### Features

* survey ([41c8203](https://gitlab.com/ydisanto/recos-diet/commit/41c8203b1061b5eeab63aa223a97ee7229c4ac96))

## [2.0.2](https://gitlab.com/ydisanto/recos-diet/compare/2.0.1...2.0.2) (2022-06-23)


### Bug Fixes

* weight input with 1 fraction digit ([5b23f4f](https://gitlab.com/ydisanto/recos-diet/commit/5b23f4f2aac44bb11bdd45deb3c6fae1f365a235))

## [2.0.1](https://gitlab.com/ydisanto/recos-diet/compare/2.0.0...2.0.1) (2022-05-23)


### Bug Fixes

* **patient-form:** enter key event regression ([0ffe528](https://gitlab.com/ydisanto/recos-diet/commit/0ffe528fe3bdeab5b437b6d6118e3a2e19f9b6d2))

# [2.0.0](https://gitlab.com/ydisanto/recos-diet/compare/1.35.2...2.0.0) (2022-05-18)


### Features

* recos-diet v2 ([1ba0adb](https://gitlab.com/ydisanto/recos-diet/commit/1ba0adbc77c457b823bba3e7955734f3d0bd28ab))


### BREAKING CHANGES

* v2

# [1.8.0](https://gitlab.com/ydisanto/recos-diet-ddd/compare/1.7.0...1.8.0) (2022-04-07)


### Bug Fixes

* rewrite rules with htaccess ([94ebec8](https://gitlab.com/ydisanto/recos-diet-ddd/commit/94ebec8c1600d70dba416d6454d841677e904c1e))


### Features

* update covid vit. D recommendation ([cbb14bc](https://gitlab.com/ydisanto/recos-diet-ddd/commit/cbb14bc6336007ff3cf34198ea481025127f4500))

# [1.7.0](https://gitlab.com/ydisanto/recos-diet-ddd/compare/1.6.0...1.7.0) (2022-04-05)


### Bug Fixes

* handset mode menu title ([b6093f4](https://gitlab.com/ydisanto/recos-diet-ddd/commit/b6093f4559307405bd6323388cc26737d4cb36cd))


### Features

* move warnings to pathologies ([f67d70d](https://gitlab.com/ydisanto/recos-diet-ddd/commit/f67d70d3d6a193828c0a46a54511c95c70f5cae8))
* **target-flows-strip:** remove 30kcal when obesity ([95dddc9](https://gitlab.com/ydisanto/recos-diet-ddd/commit/95dddc9232c919d07f7a0d36cb6a36b82ad4d70f))
* update denutrition ([f4b7f79](https://gitlab.com/ydisanto/recos-diet-ddd/commit/f4b7f7972f865aaee9cd4807ef81bd76e3f0959c))
* update denutrition source for old patients ([61d62ce](https://gitlab.com/ydisanto/recos-diet-ddd/commit/61d62ce53c3d706e5ea6d1ae0fc149261ac144a6))
* update products ([0ff4d8c](https://gitlab.com/ydisanto/recos-diet-ddd/commit/0ff4d8c294d05f0be57e7d911c1225059a808bf7))

# [1.6.0](https://gitlab.com/ydisanto/recos-diet-ddd/compare/1.5.0...1.6.0) (2022-02-24)


### Bug Fixes

* **accessibility:** improve footer contrast ([9056193](https://gitlab.com/ydisanto/recos-diet-ddd/commit/9056193d9c12dfb22bd49845d23dbbbec5c2601e))
* IMC denutrition criteria ([38d922a](https://gitlab.com/ydisanto/recos-diet-ddd/commit/38d922a8980e08b3b1c4f782d04f6d86f15b158a))
* set page lang to fr ([8378735](https://gitlab.com/ydisanto/recos-diet-ddd/commit/8378735b41cab066260fc2d841126645b3302a66))


### Features

* app update ([f62d9fa](https://gitlab.com/ydisanto/recos-diet-ddd/commit/f62d9fad928e6dc95e49671604bb02f12c50235f))

# [1.5.0](https://gitlab.com/ydisanto/recos-diet-ddd/compare/1.4.0...1.5.0) (2021-09-27)


### Bug Fixes

* handset flying menu fully ([a117ee9](https://gitlab.com/ydisanto/recos-diet-ddd/commit/a117ee9424cafd48f7373ba867ac522f077df010))


### Features

* merging target flows and micronutriments ([2dec2a2](https://gitlab.com/ydisanto/recos-diet-ddd/commit/2dec2a2f9db63f61aa45e4704f2da95c11e435da))
* products page ([e12284b](https://gitlab.com/ydisanto/recos-diet-ddd/commit/e12284b060efe7ae640e990f22f073813f32fe8b))

# [1.4.0](https://gitlab.com/ydisanto/recos-diet-ddd/compare/1.3.1...1.4.0) (2021-09-17)


### Features

* pathology warnings ([a63c4f8](https://gitlab.com/ydisanto/recos-diet-ddd/commit/a63c4f889370ea04aa30271ccd4f4378f6ea3ae0))

## [1.3.1](https://gitlab.com/ydisanto/recos-diet-ddd/compare/1.3.0...1.3.1) (2021-09-02)


### Bug Fixes

* **footer:** make badge more readable ([d41026c](https://gitlab.com/ydisanto/recos-diet-ddd/commit/d41026c3716648d7ca3c11e4602b9efd6b52b6d0))

# [1.3.0](https://gitlab.com/ydisanto/recos-diet-ddd/compare/1.2.1...1.3.0) (2021-09-02)


### Bug Fixes

* remove target flows strip center justification ([d3ead68](https://gitlab.com/ydisanto/recos-diet-ddd/commit/d3ead685dc7212d2a1ac11371bdafedfaa583cc3))
* rename micronutriment to micronutriment ([47c3190](https://gitlab.com/ydisanto/recos-diet-ddd/commit/47c3190c4bf937eb883243216495db4623b8e92d))


### Features

* highlight bibliography entry ([a3ae86e](https://gitlab.com/ydisanto/recos-diet-ddd/commit/a3ae86e64281ddf34d881b7c12205fc87f637378))
* nutritionist opinion requirement warning ([c6b0177](https://gitlab.com/ydisanto/recos-diet-ddd/commit/c6b0177b960fa1d0c384e3d4b63260529936b5e6))

## [1.2.1](https://gitlab.com/ydisanto/recos-diet-ddd/compare/1.2.0...1.2.1) (2021-08-01)


### Bug Fixes

* nutriments data assets ([d4239e5](https://gitlab.com/ydisanto/recos-diet-ddd/commit/d4239e533fadb6ab880e9bebbe62a464330a21c6))

# [1.2.0](https://gitlab.com/ydisanto/recos-diet-ddd/compare/1.1.0...1.2.0) (2021-08-01)


### Features

* handset sticky menu ([c6ac12f](https://gitlab.com/ydisanto/recos-diet-ddd/commit/c6ac12f9510951e067211e1dd36c1f79f929e72a))
* rename recommendations to nutriments ([a6f6a43](https://gitlab.com/ydisanto/recos-diet-ddd/commit/a6f6a43dfebd8de084e0bbb11ef49fb757c25188))

# [1.1.0](https://gitlab.com/ydisanto/recos-diet-ddd/compare/1.0.0...1.1.0) (2021-07-31)


### Bug Fixes

* use proper events type name ([654cf11](https://gitlab.com/ydisanto/recos-diet-ddd/commit/654cf117fc81d4101d2505bf27204127ceda459f))
* wrong domain import ([be686f0](https://gitlab.com/ydisanto/recos-diet-ddd/commit/be686f0fa2ded10674b826ac19020db63bd4e3ed))


### Features

* nutriment exclusions ([f7944b3](https://gitlab.com/ydisanto/recos-diet-ddd/commit/f7944b3c5f994e77cc33470fb0ec9554bb6e2f1d))

# 1.0.0 (2021-07-31)


### Bug Fixes

* center pane width ([3bbbf09](https://gitlab.com/ydisanto/recos-diet-ddd/commit/3bbbf096a8d996b539602aba8072a5bb3bdcfe2f))


### Features

* about page ([eb43af0](https://gitlab.com/ydisanto/recos-diet-ddd/commit/eb43af0b5ee5883e6b065a81e88553ae090321c6))
* add disclaimer ([fe6bcac](https://gitlab.com/ydisanto/recos-diet-ddd/commit/fe6bcacbd6dd73b42dc9325ed49505b970944127))
* add patient form ([2145480](https://gitlab.com/ydisanto/recos-diet-ddd/commit/2145480384e916e938cfafdcd93911046b87917f))
* add sidenav ([8edfb8a](https://gitlab.com/ydisanto/recos-diet-ddd/commit/8edfb8ae0663f3bdf6adbb65a2ce84b4e1fcc0e1))
* add wip image ([c77d68f](https://gitlab.com/ydisanto/recos-diet-ddd/commit/c77d68ff4161924c96b11a84b7a623511aa9f7a9))
* bibliography ([02d751e](https://gitlab.com/ydisanto/recos-diet-ddd/commit/02d751e618c731d506505ee143ba165128fe6bc8))
* denutrition ([6a9a52f](https://gitlab.com/ydisanto/recos-diet-ddd/commit/6a9a52f6752daccf8efa1756c156782a497c4fbf))
* flows strip ([daec266](https://gitlab.com/ydisanto/recos-diet-ddd/commit/daec266eef6eac21dadc83aa50d750d75f34d303))
* footer and about page (wip) ([041c6ce](https://gitlab.com/ydisanto/recos-diet-ddd/commit/041c6ce080a216fb3efa05876ac8954fc3c02325))
* nutrition ([d877923](https://gitlab.com/ydisanto/recos-diet-ddd/commit/d8779231a8eaab650fa6afec9f9024ca4f294e6e))
* pathologies ([5a47f50](https://gitlab.com/ydisanto/recos-diet-ddd/commit/5a47f5091ee3ecf5ce0f456cb6d605803dd25a6a))
* recos ([dff2710](https://gitlab.com/ydisanto/recos-diet-ddd/commit/dff2710ffe43231bf2de135e35ac50c9f29cada7))
* sri ([f531ce8](https://gitlab.com/ydisanto/recos-diet-ddd/commit/f531ce8cfdee0e5762430cb98ffc37f2d06b4ba7))
* target flows ([269f87d](https://gitlab.com/ydisanto/recos-diet-ddd/commit/269f87dd0154c90ae8442d4ffda8bd3eefeb4f7a))
* update footer ([a5eb96a](https://gitlab.com/ydisanto/recos-diet-ddd/commit/a5eb96a2e89a176892e44b3cd96e2db75215d0dc))
* **bibliography:** add bibliography link navigation ([1ff46eb](https://gitlab.com/ydisanto/recos-diet-ddd/commit/1ff46ebfa586516b37f54c8a8afa45ce48b115a2))
* **menu:** add selected menu label for handset ([0a3028b](https://gitlab.com/ydisanto/recos-diet-ddd/commit/0a3028b823d78691cc402fcc686f277fe73e1a68))
